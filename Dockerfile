FROM rust:buster

RUN rustup component add rustfmt
RUN rustup component add clippy
RUN apt update && apt install -y gcc-mingw-w64 build-essential zlib1g-dev gcc-aarch64-linux-gnu 

RUN apt install -y openssl libssl-dev 
RUN openssl version -a

# Update the shared library cache
RUN ldconfig

# Setup rustup

RUN rustup target add x86_64-pc-windows-gnu
RUN rustup target add x86_64-unknown-linux-gnu
RUN rustup target add aarch64-apple-darwin
RUN rustup target add aarch64-unknown-linux-gnu
RUN rustup target add aarch64-pc-windows-msvc

# Install cross (and a docker client as it will be needed)
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-get install -y software-properties-common apt-transport-https ca-certificates curl gnupg2
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
RUN apt-get update
RUN apt-get install -y docker-ce docker-ce-cli containerd.io
RUN cargo install --git="https://github.com/rust-embedded/cross.git" --branch="main" cross


